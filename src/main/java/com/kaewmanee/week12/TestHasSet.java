package com.kaewmanee.week12;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Collections;

public class TestHasSet {
    public static void main(String[] args) {
        HashSet<String> set = new HashSet();
        set.add("A1");
        set.add("A2");
        set.add("A3");
        printSet(set);

        set.add("A1");
        printSet(set);
        System.out.println(set.contains("A1"));
        set.remove("A1");
        System.out.println(set);
        set.add("A1");

        HashSet<Integer> set2 = new HashSet<>();
        set2.add(1);
        set2.add(2);
        set2.add(3);
        System.out.println(set2);
        set2.clear();
        System.out.println(set2);

        HashSet<String> set3 = new HashSet<>();
        set3.addAll(set);
        System.out.println(set3);
        

        
    }

    private static void printSet(HashSet<String> set) {
        Iterator<String> iter = set.iterator();
        while(iter.hasNext()) {
            System.out.println(iter.next());
        }
        System.out.println();
    }
}
